# app/tests/test_user_model.py

import pytest

from db.csv_handler import save_user_to_csv, get_user, update_trade, delete_trade
from models.user import User
from routers.trades import create_trade

mock_trade_data = {
    "timestamp": 1633027200,
    "chain": "ethereum",
    "user": "0x123",
    "wallet": "0x123",
    "tradeID": "0xabc",
    "tradeType": "limitBuy",
    "asset": "BTC",
    "amount": 1,
    "beforePrice": 40000,
    "executionPrice": 40500,
    "finalPrice": 40500,
    "tradedAmount": 1,
    "executionFee": 0.3,
    "transactionFee": 0.5
}


@pytest.mark.asyncio
async def test_get_existing_user():
    """
    Test for fetching an existing user.
    """
    # Setup: Save a dummy user to CSV
    await save_user_to_csv(User(username="testuser", email="test@example.com"))

    # Exercise: Fetch the user
    fetched_user = await get_user("testuser")

    # Verify: Check if the fetched user exists and has the correct username
    assert fetched_user is not None
    assert fetched_user.username == "testuser"


@pytest.mark.asyncio
async def test_get_non_existing_user():
    """
    Test for fetching a non-existing user.
    """
    # Exercise: Try to fetch a non-existing user
    fetched_user = await get_user("nonexistentuser")

    # Verify: Check if the fetched user is None
    assert fetched_user is None


@pytest.mark.asyncio
async def test_create_new_trade():
    """
    Test for creating a new trade.
    """
    # Simulate the creation of a new trade
    created_trade = await create_trade(mock_trade_data)
    assert created_trade == mock_trade_data


@pytest.mark.asyncio
async def test_update_existing_trade():
    """
    Test for updating an existing trade.
    """
    # Simulate updating an existing trade
    updated_data = {"executionPrice": 41000, "finalPrice": 41000}
    updated_trade = await update_trade("0xabc", updated_data)
    assert updated_trade["executionPrice"] == 41000


@pytest.mark.asyncio
async def test_delete_trade():
    """
    Test for deleting a trade.
    """
    # Simulate deleting a trade
    deleted_trade = await delete_trade("0xabc")
    assert deleted_trade is None
