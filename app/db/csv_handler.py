# app/db/csv_handler.py

import csv
import logging
import os
from typing import Dict, Optional, List

import aiofiles

from models.trade import Trade
from models.user import User

USER_DATABASE_CSV_PATH = os.getenv("USER_DATABASE_CSV_PATH", "users.csv")
USER_DATABASE_CSV_HEADERS = os.getenv("USER_DATABASE_CSV_HEADERS", "username,email,full_name,disabled").split(',')

TRADE_DATABASE_CSV_PATH = os.getenv("TRADE_DATABASE_CSV_PATH", "trades.csv")
TRADE_DATABASE_CSV_HEADERS = os.getenv(
    "TRADE_DATABASE_CSV_HEADERS",
    "timestamp,chain,user,wallet,tradeID,tradeType,asset,amount,beforePrice,executionPrice,finalPrice,tradedAmount,executionFee,transactionFee"
).split(',')

# Initialize logging
logging.basicConfig(level=logging.INFO)


async def read_csv_async(file_path: str):
    """
    Asynchronously read a CSV file and return its content.
    """
    async with aiofiles.open(file_path, mode='r') as file:
        content = await file.read()
    return list(csv.reader(content.strip().split("\n")))


async def write_csv_async(file_path: str, rows: List[List[str]]):
    """
    Asynchronously write a list of rows to a CSV file.
    """
    content = "\n".join([",".join(map(str, row)) for row in rows])
    async with aiofiles.open(file_path, mode='w') as file:
        await file.write(content)


# User CSV file operations
async def get_user(username: str):
    """
    Asynchronously fetches a user by username using the get_user_from_csv function.
    If the user exists, returns a User model populated with the fetched data.
    If the user does not exist, returns None.
    """
    user_data = await get_user_from_csv(username)
    if user_data:
        return User(**user_data)
    return None


async def save_user_to_csv(user: User) -> bool:
    """
    Asynchronously saves a user to the users CSV file.

    Args:
        user (User): The user object to save.

    Returns:
        bool: True if the operation was successful, False otherwise.
    """
    try:
        async with aiofiles.open(USER_DATABASE_CSV_PATH, mode='a') as file:
            # Check if the file is empty and write headers if it is
            if await file.tell() == 0:
                await file.write(','.join(USER_DATABASE_CSV_HEADERS) + '\n')

            # Write the user data
            user_data = [
                user.username, user.email, user.full_name, user.disabled
            ]
            await file.write(','.join(map(str, user_data)) + '\n')

        logging.info("User saved successfully.")
        return True
    except Exception as e:
        logging.error(f"Error saving user: {e}")
        return False


async def get_user_from_csv(username: str) -> Optional[Dict[str, str]]:
    """
    Asynchronously retrieves a user from the users CSV file based on the username.
    """
    rows = await read_csv_async(USER_DATABASE_CSV_PATH)
    for row in csv.DictReader(rows):
        if row["username"] == username:
            return row
    return None


async def get_users_from_csv() -> list[list[str]]:
    """
    Asynchronously retrieves a user from the users CSV file based on the username.
    """
    return await read_csv_async(USER_DATABASE_CSV_PATH)


# Trades CSV file operations
async def save_trade(trade: Trade) -> bool:
    """
    Asynchronously saves a trade to the trades CSV file.
    """
    try:
        rows = await read_csv_async(TRADE_DATABASE_CSV_PATH)
        if not rows:
            rows = [TRADE_DATABASE_CSV_HEADERS]
        rows.append(
            [
                trade.timestamp, trade.chain, trade.user, trade.wallet, trade.tradeID,
                trade.tradeType, trade.asset, trade.amount, trade.beforePrice,
                trade.executionPrice, trade.finalPrice, trade.tradedAmount,
                trade.executionFee, trade.transactionFee
            ]
        )
        await write_csv_async(TRADE_DATABASE_CSV_PATH, rows)
        return True
    except Exception as e:
        logging.error(f"Error saving trade: {e}")
        return False


async def get_trades() -> List[Trade]:
    """
    Asynchronously fetches all trades from the trades CSV file.
    """
    rows = await read_csv_async(TRADE_DATABASE_CSV_PATH)
    return [Trade.parse_obj(row) for row in csv.DictReader(rows)]


async def get_trade_by_id(trade_id: str) -> Optional[Trade]:
    """
    Asynchronously fetches a single trade by its ID from the trades CSV file.
    """
    rows = await read_csv_async(TRADE_DATABASE_CSV_PATH)
    for row in csv.DictReader(rows):
        if row[4] == trade_id:
            return Trade.parse_obj(row)
    return None


async def update_trade(trade_id: str, trade: Trade) -> None:
    """
    Asynchronously updates a specific trade in the trades CSV file.
    """
    rows = await read_csv_async(TRADE_DATABASE_CSV_PATH)
    updated_rows = []
    for row in csv.DictReader(rows):
        if row['tradeID'] == trade_id:
            updated_rows.append(list(trade.dict().values()))
        else:
            updated_rows.append(list(row.values()))
    await write_csv_async(TRADE_DATABASE_CSV_PATH, [TRADE_DATABASE_CSV_HEADERS] + updated_rows)


async def delete_trade(trade_id: str) -> None:
    """
    Asynchronously deletes a specific trade from the trades CSV file.
    """
    rows = await read_csv_async(TRADE_DATABASE_CSV_PATH)
    updated_rows = [row for row in csv.DictReader(rows) if row['tradeID'] != trade_id]
    await write_csv_async(TRADE_DATABASE_CSV_PATH, [TRADE_DATABASE_CSV_HEADERS] + updated_rows)
