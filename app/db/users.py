# app/db/users.py
import os
from datetime import datetime, timedelta

from dotenv import load_dotenv
from jose import jwt

from db.csv_handler import get_user

load_dotenv()

SECRET_KEY = os.getenv("SECRET_KEY")
ALGORITHM = os.getenv("ALGORITHM")


async def verify_password(username: str, password: str) -> bool:
    """
    Asynchronously verifies the provided password against the stored password.
    """
    user = await get_user(username)
    return bool(user and user.password == password)


async def create_access_token(data: dict, expires_delta: timedelta = None):
    """
    Asynchronously creates a JWT access token.
    """
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
    return encoded_jwt


async def authenticate_user(username: str, password: str):
    """
    Asynchronously authenticates a user based on the username and password.
    """
    user = await get_user(username)
    if user and await verify_password(username, password):
        return user
    return None
