# app/main.py

from fastapi import FastAPI
from fastapi.responses import RedirectResponse

from routers import trades, users

app = FastAPI()


@app.get("/", include_in_schema=False)
def index():
    return RedirectResponse(url="/redoc")


app.include_router(trades.router, prefix="/api/v1")
app.include_router(users.router, prefix="/api/v1")
