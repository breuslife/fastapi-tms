# app/routers/trades.py

from datetime import datetime
from typing import List

from fastapi import APIRouter, HTTPException, Depends

from db.csv_handler import save_trade, get_trades, get_trade_by_id, update_trade, delete_trade
from models.trade import Trade, Asset, TradeType
from models.user import User
from routers.authentication import get_current_active_user

# Initialize the FastAPI router
router = APIRouter()


# Create a new trade
@router.post("/trades/", response_model=Trade)
async def create_trade(trade: Trade, current_user: User = Depends(get_current_active_user)):
    """
    Asynchronously creates a new trade.
    """
    trade.timestamp = int(datetime.now().timestamp())
    success = await save_trade(trade)
    if not success:
        raise HTTPException(status_code=400, detail="Failed to create trade")
    return trade


# Get all trades
@router.get("/trades/", response_model=List[Trade])
async def get_all_trades():
    """
    Asynchronously fetches all trades.
    """
    return await get_trades()


# Get a single trade by ID
@router.get("/trades/{tradeID}/", response_model=Trade)
async def get_single_trade(trade_id: str):
    """
    Asynchronously fetches a single trade by its ID.
    """
    return await get_trade_by_id(trade_id)


# Update a trade
@router.put("/trades/{tradeID}/", response_model=Trade)
async def update_existing_trade(trade_id: str, trade: Trade):
    """
    Asynchronously updates an existing trade.
    """
    await update_trade(trade_id, trade)
    return trade


# Delete a trade
@router.delete("/trades/{tradeID}/")
async def remove_trade(trade_id: str):
    """
    Asynchronously deletes a trade.
    """
    await delete_trade(trade_id)


# Calculate daily profit or loss for a user
@router.get("/trades/profit_loss/daily/{user}/")
async def calculate_daily_profit_loss(user: str):
    """
    Asynchronously calculates daily profit or loss for a user.
    """
    trades = await get_trades()
    # Filtering trades for the specific user
    user_trades = [trade for trade in trades if trade.user == user]
    # Logic for calculating daily profit/loss
    # Assuming trade.finalPrice and trade.executionPrice have the necessary data
    daily_profit_loss = sum(trade.finalPrice - trade.executionPrice for trade in user_trades)
    return {"daily_profit_loss": daily_profit_loss}


# Calculate cumulative fees for a user
@router.get("/trades/profit_loss/cumulative_fees/{user}/")
async def calculate_cumulative_fees(user: str):
    """
    Asynchronously calculates cumulative fees for a user.
    """
    trades = await get_trades()
    # Filtering trades for the specific user
    user_trades = [trade for trade in trades if trade.user == user]
    # Logic for calculating cumulative fees
    # Assuming trade.executionFee and trade.transactionFee have the necessary data
    total_fees = sum(trade.executionFee + trade.transactionFee for trade in user_trades)
    return {"total_fees": total_fees}


# Calculate daily profit or loss for a user by asset or trade type
@router.get("/trades/profit_loss/daily_by_asset/{user}/")
async def calculate_daily_profit_loss_by_asset(user: str, asset: Asset = None, trade_type: TradeType = None):
    """
    Asynchronously calculates daily profit or loss for a user by asset or trade type.
    """
    trades = await get_trades()
    # Filtering trades for the specific user and optional asset and trade type
    user_trades = [trade for trade in trades if trade.user == user]
    if asset:
        user_trades = [trade for trade in user_trades if trade.asset == asset]
    if trade_type:
        user_trades = [trade for trade in user_trades if trade.tradeType == trade_type]
    # Logic for calculating daily profit/loss by asset or trade type
    daily_profit_loss = sum(trade.finalPrice - trade.executionPrice for trade in user_trades)
    return {"daily_profit_loss_by_asset_or_type": daily_profit_loss}


# Calculate slippage cost for a user
@router.get("/trades/slippage_cost/{user}/")
async def calculate_slippage_cost(user: str):
    """
    Asynchronously calculates slippage cost for a user.
    """
    trades = await get_trades()
    # Filtering trades for the specific user
    user_trades = [trade for trade in trades if trade.user == user]
    # Logic for calculating slippage cost
    # Assuming trade.finalPrice and trade.beforePrice have the necessary data
    total_slippage = sum(trade.finalPrice - trade.beforePrice for trade in user_trades)
    return {"total_slippage": total_slippage}


@router.post("/calculate_slippage/")
async def calculate_slippage(trades: List[Trade]):
    slippage_list = []
    for trade in trades:
        slippage = abs((trade.execution_price - trade.expected_price) / trade.expected_price) * 100
        slippage_list.append(slippage)
    return {"slippages": slippage_list}
