# app/routers/users.py

from typing import List

from fastapi import APIRouter, HTTPException

from db.csv_handler import get_users_from_csv, save_user_to_csv
from models.user import User

router = APIRouter()


@router.get("/users/", response_model=List[User])
async def get_users():
    """
    Get all users
    """
    users = await get_users_from_csv()
    if not users:
        raise HTTPException(status_code=404, detail="No users found")
    return users


@router.post("/users/", response_model=User)
async def create_user(user_input: User):
    """
    Create a new user
    """
    user = User(
        username=user_input.username,
        email=user_input.email,
        full_name=user_input.full_name,
        disabled=user_input.disabled
    )
    is_saved = await save_user_to_csv(user)
    if not is_saved:
        raise HTTPException(status_code=500, detail="Could not save user")
    return user
