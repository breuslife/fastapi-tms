# app/models/user.py

from typing import Optional

from pydantic import BaseModel


class User(BaseModel):
    """
    User model representing the user entity.
    """
    username: str  # Username of the user
    email: Optional[str] = None  # Optional email of the user
    full_name: Optional[str] = None  # Optional full name of the user
    disabled: Optional[bool] = None  # Optional field to indicate if the user is disabled
