# app/models/trade.py

from enum import Enum

from pydantic import BaseModel


class Chain(str, Enum):
    """
    Enumeration for different blockchain chains.
    """
    ethereum = "ethereum"
    arbitrum = "arbitrum"
    optimism = "optimism"
    polygon = "polygon"


class TradeType(str, Enum):
    """
    Enumeration for different types of trades.
    """
    limitBuy = "limitBuy"
    limitSell = "limitSell"
    marketBuy = "marketBuy"
    marketSell = "marketSell"


class Asset(str, Enum):
    """
    Enumeration for different types of assets.
    """
    BTC = "BTC"
    ETH = "ETH"
    XRP = "XRP"
    XLM = "XLM"
    DOGE = "DOGE"


class Trade(BaseModel):
    """
    Trade model that holds information about a single trade.
    """
    timestamp: int  # Unix timestamp of when the trade occurred
    chain: Chain  # The blockchain chain on which the trade occurred
    user: str  # The username of the person who made the trade
    wallet: str  # The wallet address used for the trade
    tradeID: str  # Unique identifier for the trade
    tradeType: TradeType  # The type of trade (e.g., limitBuy, limitSell)
    asset: Asset  # The asset being traded (e.g., BTC, ETH)
    amount: int  # The amount of the asset being traded
    beforePrice: float  # The asset price before the trade
    executionPrice: float  # The asset price at which the trade was executed
    finalPrice: float  # The final asset price after the trade
    tradedAmount: float  # The amount of the asset that was actually traded
    executionFee: float  # Fee for executing the trade
    transactionFee: float  # Additional transaction fee
