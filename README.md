# Trade Management System (TMS)

---
`Docker`, `Docker-compose`, `FastAPI`, `CSV`, `ReDoc`

---

## Overview

This project is a trade management system built with `FastAPI`. It provides a `RESTful API` for **tracking**, **managing
**, and
**analyzing trades**, as well as calculating profit and loss for traders over time.

## Prerequisites

- `Docker`
- `Docker Compose`

## Setup & Installation

1. Clone the repository:

```bash
git clone https://gitlab.com/breuslife/fastapi-tms.git
```

2. Navigate to the project directory:

```bash
cd tms
```

3. Build and start the Docker containers:

```bash
docker-compose up --build
```

This will **build** and **start** the `FastAPI` application on port **8000** service as specified in
the `docker-compose.yml` file with **Python3.11**.

## Usage

You can interact with the **API** using tools like `curl` or `Postman`.

## API Documentation

Once the application is running, you can visit `http://localhost:8000/docs` for the auto-generated `FastAPI`
documentation or `http://localhost:8000/redoc`.

Here you can find details about each **API endpoint**, as well as try out
the **API** calls directly from the browser.

## API Endpoints

- **POST `/trades/`**: Create a new trade
- **GET `/trades/{trade_id}`**: Retrieve a specific trade by its ID
- **PUT `/trades/{trade_id}`**: Update a specific trade
- **GET `/trades/`**: List all trades
- **DELETE `/trades/{trade_id}`**: Delete a specific trade

---

## Tests

To run the tests, navigate to the project directory and run:

```bash
docker-compose exec fastapi pytest tests
```

----------------

## Структура проекту

```
app/: Головний каталог додатка
models/: Моделі даних
routers/: Маршрутизація API
db/: Робота з CSV-файлами
tests/: Тестові сценарії
main.py: Головний файл додатка
```

## Маршрути для обчислення

Додамо наступні маршрути для обчислення:

```
GET /trades/profit_loss/daily/{user}/: Обчислення щоденного прибутку/збитків для користувача
GET /trades/profit_loss/cumulative_fees/{user}/: Обчислення сукупних комісій, сплачених користувачем
GET /trades/profit_loss/daily_by_asset/{user}/: Обчислення щоденного прибутку/збитків за активом або типом торгівлі
```
