# Use an official Python runtime as a parent image
FROM python:3.11-slim-buster

# Set the working directory in the container to /code
WORKDIR /code

# Install required packages
COPY ./requirements.txt /code/
RUN pip install --no-cache-dir -r requirements.txt

# Copy the current directory contents into the container at /code
COPY ./app /code/

# Make port 8000 available to the world outside this container
EXPOSE 8000
